package com.company;

import java.io.*;
import java.util.List;
import java.util.Scanner;

/**
 * Created by nikoroo on 3/22/16.
 */
public class PaymentMain {

    private List<Payment> payments;
    private String fileName;

    public PaymentMain(List<Payment> payments, String fileName) {
        this.payments = payments;
        this.fileName = fileName;
    }

    /**
     * Nacitanie platieb zo suboru
     */
    public void readFromFile() {
        BufferedReader bufferedReader = null;
        try {
            String line = "";
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split("\\s+");
                payments.add(new Payment(split[0], Integer.parseInt(split[1])));
            }
        } catch (IOException e) {
            System.out.println("Chyba pri nacitani zo suboru");
            System.exit(0);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Hlavna slucka programu pre vstup od pouzivatela
     *
     * @param paymentScheduler
     */
    public void userInputLoop(PaymentScheduler paymentScheduler) {
        BufferedWriter output = null;
        try {
            Scanner scanner = new Scanner(System.in);
            String line = "";
            output = new BufferedWriter(new FileWriter(fileName, true));
            while ((line = scanner.nextLine()) != null) {
                String[] split = line.split("\\s+");
                if (line.equals("quit")) {
                    paymentScheduler.stopPaymentScheduler();
                    break;
                } else {
                    int amount = 0;
                    if (split[0].length() == 3) {
                        try {
                            amount = Integer.parseInt(split[1]);
                        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                            System.out.println("Zly format!\n");
                            continue;
                        }
                    } else {
                        System.out.println("Zly format!\n");
                        continue;
                    }
                    System.out.println();
                    output.append(split[0] + " " + amount + "\n");
                    payments.add(new Payment(split[0], amount));
                }
            }
        } catch (IOException e) {
            System.out.println("Chyba pri zapisovani do suboru.");
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
