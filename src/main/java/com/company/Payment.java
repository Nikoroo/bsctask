package com.company;

/**
 * Created by nikoroo on 3/21/16.
 */
public class Payment {
    private String currency;
    private int amount;

    public Payment(String currency, int amount) {
        this.setCurrency(currency);
        this.setAmount(amount);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return getCurrency() + " " + getAmount();
    }
}
