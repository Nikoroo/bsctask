package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * Hlavna metoda
     *
     * @param args args[0]=nazov_suboru
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 1 || args[0].isEmpty()) {
            System.out.println("Nebol zadany subor!");
            System.exit(0);
        }
        System.out.println("Welcome, this is Payment Tracker.\n");

        // Nacitanie kurzov mien z internetu
        ExchangeRates.getInstance().fillCurrencyRates();

        List<Payment> payments = new ArrayList<>();

        PaymentMain paymentMain = new PaymentMain(payments, args[0]);
        paymentMain.readFromFile();

        // Spustenie vypisu v pravidelnych intervaloch
        PaymentScheduler paymentScheduler = new PaymentScheduler(payments);
        paymentScheduler.startPaymentScheduler();

        // Nacitavanie vstupu od pouzivatela
        paymentMain.userInputLoop(paymentScheduler);


    }
}
