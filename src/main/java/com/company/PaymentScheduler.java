package com.company;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by nikoroo on 3/21/16.
 */
public class PaymentScheduler {
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private List<Payment> payments;
    // Mozno by som dal ConcurrentHashMap pre thread safety a nejake synchronize bloky pre pristup k suboru
    private Map<String, Float> exchangeRates = ExchangeRates.getInstance().getCurrencies();

    public PaymentScheduler(List payments) {
        this.payments = payments;
    }

    /**
     * Spusti scheduler v pravidelnych intervaloch vypisuje platby
     */
    public void startPaymentScheduler() {
        executorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                for (Payment payment : payments) {
                    if (payment.getAmount() == 0) {
                        continue;
                    }
                    Float exchangeRate = exchangeRates.get(payment.getCurrency());
                    String exchangeString = "";
                    if (exchangeRate != null) {
                        exchangeString = " (USD " + String.format("%.2f", payment.getAmount() / exchangeRate) + ")";
                    }
                    System.out.println(payment + exchangeString);
                }
                System.out.println();
            }
        }, 5, 60, TimeUnit.SECONDS);
    }

    /**
     * Zastavi scheduler
     */
    public void stopPaymentScheduler() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
