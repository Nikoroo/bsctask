package com.company;

import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nikoroo on 3/22/16.
 */
public class ExchangeRates {
    private static final Map<String, Float> currencies = new HashMap<>();

    private static final ExchangeRates exchangeRates = new ExchangeRates();

    private ExchangeRates() {
    }

    public static ExchangeRates getInstance() {
        return exchangeRates;
    }

    public Map<String, Float> getCurrencies() {
        return currencies;
    }

    /**
     * Ak sa zmeni html alebo ajvascript na stranke samozrejme to moze prestat fungovat
     *
     * @throws IOException
     */
    public void fillCurrencyRates() {
        System.out.println("Nacitavam aktualne kurzy mien z internetu ...");

        WebDriver driver = new SilentHtmlUnitDriver();
        driver.get("http://www.xe.com/currencytables/?from=USD");

        WebElement tableBody = driver.findElement(By.id("historicalRateTbl")).findElement(By.tagName("tbody"));
        List<WebElement> rows = tableBody.findElements(By.tagName("tr"));

        for (int i = 1; i < rows.size(); i++) {
            WebElement row = rows.get(i);
            List<WebElement> cols = row.findElements(By.tagName("td"));

            getCurrencies().put(cols.get(0).getText(), Float.parseFloat(cols.get(2).getText()));
        }

        System.out.println("Kurzi mien uspesne nacitane.");
    }

    private class SilentHtmlUnitDriver extends HtmlUnitDriver {
        SilentHtmlUnitDriver() {
            super();
            this.getWebClient().setCssErrorHandler(new SilentCssErrorHandler());
        }
    }
}
